Git Basics:
Git is version-control software that makes collaboration with teammates super simple.
Git lets you easily keep track of every revision you and your team make during the development of your software.

How to use:
Create a "repository" (project) with a git hosting tool (like Bitbucket)
Copy (or clone) the repository to your local machine.
Add a file to your local repo and "commit" (save) the changes.
"Push" your changes to your master branch.

Push Pull
The git push command is used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repo. It's the counterpart to git fetch , but whereas fetching imports commits to local branches, pushing e
xports commits to remote branches.