Gitlab Basics:

Create and add your SSH public key, for enabling Git over SSH.
Create a project, to start using GitLab.
Create a group, to combine and administer projects together.
Create a branch, to make changes to files stored in a project’s repository.
Feature branch workflow.
Fork a project, to duplicate projects so they can be worked on in parallel.
Add a file, to add new files to a project’s repository.
Create an issue, to start collaborating within a project.
Create a merge request, to request changes made in a branch be merged into a project’s repository.
See how these features come together in the GitLab Flow introduction video and GitLab Flow page.
Working with Git from the command line